#lang racket

;; Ex 3.42
;;

;;  It’s a safe change to make, since everything is ultimately handled by the same serialiser. 
;;  Instead of having a new feed to pass procedure calls into the serialiser every time 
;;  the mutating procedures is called, two separate feeds are created, 
;;  one for deposits and one for withdrawals.

  

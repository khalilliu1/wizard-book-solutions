#lang racket

;; Ex 3.24
;;
(require scheme/mpair)

(define (mcaar xs) (mcar (mcar xs)))
(define (mcadr xs) (mcar (mcdr xs)))
(define (mcdar xs) (mcdr (mcar xs)))
(define (mcddr xs) (mcdr (mcdr xs))) 

(define (make-table same-key?)
  (define local-table (mlist '*table*))
  
  (define (assoc key records)
    (cond ((null? records) #f)
          ((same-key? key (mcaar records)) (mcar records))
          (else (assoc key (mcdr records)))))
  
  (define (lookup key-1 key-2)
    (let ((subtable (assoc key-1 (mcdr local-table))))
      (if subtable
          (let ((record (assoc key-2 (mcdr subtable))))
            (if record
                (mcdr record)
                #f))
          #f)))
  
  (define (insert! key-1 key-2 value)
    (let ((subtable (assoc key-1 (mcdr local-table))))
      (if subtable
          (let ((record (assoc key-2 (mcdr subtable))))
            (if record
                (set-mcdr! record value)
                (set-mcdr! subtable
                           (mcons (mcons key-2 value)
                                  (mcdr subtable)))))
          (set-mcdr! local-table
                     (mcons (mlist key-1
                                  (mcons key-2 value))
                            (mcdr local-table)))))
    'ok)    
  (define (dispatch m)
    (cond ((eq? m 'lookup) lookup)
          ((eq? m 'insert!) insert!)
          (else (error "Unknown operation -- TABLE" m))))
  dispatch)

#lang racket

(require "lib/stream.rkt")

;; Exercise 3.54
;;
(define (square n) (* n n))
(define (divisible? x y) (= (remainder x y) 0))
(define (prime? n)
  (define (iter ps)
    (cond ((> (square (stream-car ps)) n) true)
          ((divisible? n (stream-car ps)) false)
          (else (iter (stream-cdr ps)))))
  (iter primes))


(define factorials 
  (cons-stream 1 
               (mul-streams factorials integers)))

(define fibs
  (cons-stream 0
               (cons-stream 1
                            (add-streams (stream-cdr fibs)
                                         fibs))))


(define primes
  (cons-stream
   2
   (stream-filter prime? (integers-starting-from 3))))



#lang racket

(require "lib/stream.rkt")

;; Exercise 3.55
;;
(define (partial-sum s)
  (cons-stream (stream-car s)
               (add-streams (stream-cdr s)
                            (partial-sum s))))

(define sum-ints (partial-sum integers))


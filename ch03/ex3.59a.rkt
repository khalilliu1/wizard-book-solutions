#lang racket

(require "lib/stream.rkt")

;; Ex 3.59a
;;
(define (integrate-series coeffs)
  (stream-map / coeffs integers))


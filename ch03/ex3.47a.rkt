#lang racket

(require racket/mpair)

;; Ex 3.47a
;;

;; Note - this will only work if we can guarantee that test-and-set! and clear! run atomically.
(define (test-and-set! cell)
  (if (mcar cell)
      true
      (begin (set-mcar! cell true)
             false)))

(define (clear! cell)
  (set-mcar! cell false))

(define (make-mutex)
  (let ((cell (mlist false)))            
    (define (the-mutex m)
      (cond ((eq? m 'acquire)
             (when (test-and-set! cell)
               (the-mutex 'acquire))) ; retry
            ((eq? m 'release) (clear! cell))))
    the-mutex))

(define (make-sempahore n)
  (local [(define count n)
          (define mutex (make-mutex))
          (define (sempahore m)
            (cond [(eq? m 'acquire)
                   (mutex 'acquire)
                   (cond [(> count 0) (set! count (- count 1))
                                      (mutex 'release)]
                         [else (mutex 'release)
                               (semaphore 'acquire)])]
                  [(eq? m 'release)
                   (mutex 'acquire)
                   (unless (= count n)
                     (set! count (+ count 1)))
                   (mutex 'release)]
    sempahore))


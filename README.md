YASICP
======

Yes another SICP solutions repository.

These solutions use [Racket](http://racket-lang.org/) but any more recent version should work.
Some of the exercises use other libraries - notably [the SICP language](http://docs.racket-lang.org/sicp-manual/index.html)
I've tried to check that all exercises actually run - wherever it's appropriate.

Caveat developor!

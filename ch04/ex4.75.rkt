#lang racket/base
(require "lib/query-eval-unique.rkt")


;; Exercise 4.75
(run-query '(unique (job ?x (computer wizard))))
;; '(unique (job ?x (computer wizard)))

(run-query '(unique (job ?x (computer programmer))))
;; '()

(run-query '(and (job ?x ?j) (unique (job ?anyone ?j))))
;; '((and (job (Aull DeWitt) (administration secretary))
;;        (unique (job (Aull DeWitt) (administration secretary))))
;;   (and (job (Cratchet Robert) (accounting scrivener))
;;        (unique (job (Cratchet Robert) (accounting scrivener))))
;;   (and (job (Scrooge Eben) (accounting chief accountant))
;;        (unique (job (Scrooge Eben) (accounting chief accountant))))
;;   (and (job (Warbucks Oliver) (administration big wheel))
;;        (unique (job (Warbucks Oliver) (administration big wheel))))
;;   (and (job (Reasoner Louis) (computer programmer trainee))
;;        (unique (job (Reasoner Louis) (computer programmer trainee))))
;;   (and (job (Tweakit Lem E) (computer technician))
;;        (unique (job (Tweakit Lem E) (computer technician))))
;;   (and (job (Bitdiddle Ben) (computer wizard))
;;        (unique (job (Bitdiddle Ben) (computer wizard)))))

(run-query '(and (supervisor ?subordinate ?boss)
                 (unique (supervisor ?other ?boss))))

;; '((and (supervisor (Cratchet Robert) (Scrooge Eben))
;;        (unique (supervisor (Cratchet Robert) (Scrooge Eben))))
;;   (and (supervisor (Reasoner Louis) (Hacker Alyssa P))
;;        (unique (supervisor (Reasoner Louis) (Hacker Alyssa P)))))

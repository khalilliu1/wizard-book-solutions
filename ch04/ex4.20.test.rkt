#lang racket/base

(require rackunit
         "ex4.20a.rkt")

;; regression tests
;(require rackunit
;         "ex4.03.test.rkt"
;         "ex4.04.test.rkt"
;         "ex4.05.test.rkt"
;         "ex4.06.test.rkt"
;         "ex4.07.test.rkt"
;         "ex4.08.test.rkt"
;         "ex4.09.test.rkt"
;         "ex4.13.test.rkt"
;         "ex4.16.test.rkt")


; Test suite

(define letrec-tests
  (test-suite
   "Tests for the meta-circular evaluator ex4.20a.rkt - letrec"
   
   (test-case 
    "test-quote"
    (test-equal? "quoted number"  2                 (interpret '(quote 2)))
    (test-equal? "quoted symbol" 'hello             (interpret '(quote hello))) 
    (test-equal? "quoted list"   '(jay wizz 2 watt) (interpret '(quote (jay wizz 2 watt))))) 
   
   (test-case 
    "Unassigned"
    (test-equal? "basic quote"
                 (interpret '(quote *unassigned*))
                 '*unassigned*))
   (test-case 
    "Mutual recursion and simultaneous definition with letrec"
    (test-equal? "Hofstadter Female and Male sequence"
                 ; http://en.wikipedia.org/wiki/Hofstadter_sequence#Hofstadter_Female_and_Male_sequences
                 (interpret '(letrec ((F (lambda (n)
                                           (if (= n 0) 1
                                               (- n (M (F (- n 1)))))))
                                      (M (lambda (n)
                                           (if (= n 0) 0
                                               (- n (F (M (- n 1))))))))
                               (F 19)))
                 12)
    (interpret '(define (is-even? x)
                  (letrec ((even?
                            (lambda (n)
                              (if (= n 0)
                                  true
                                  (odd? (- n 1)))))
                           (odd?
                            (lambda (n)
                              (if (= n 0)
                                  false
                                  (even? (- n 1)))))))
                  (even? x)))
    (test-false "Text if 7 is even - mutually recursive definition using letrec"
                (interpret '(is-even? 7)))
    (test-true "Text if 100 is even - mutually recursive definition using letrec"
               (interpret '(is-even? 100)))
    (test-equal? "factorial can call itself from within a letrec"
                 (interpret '(letrec ((fact
                                       (lambda (n)
                                         (if (= n 1)
                                             1
                                             (* n (fact (- n 1)))))))
                               (fact 10)))
                 3628800))
   ))



(require rackunit/text-ui)
(run-tests letrec-tests 'normal)
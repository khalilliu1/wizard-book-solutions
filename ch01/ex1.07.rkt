#lang racket

;;;  Ex 1.7
;;

; good-enough? isn't accurate for values much smaller than the tolerance level.
(define (improve guess x)
  (average guess (/ x guess)))

(define (average x y)
  (/ (+ x y) 2))

(define (sqrt-gap x)
  (sqrt-iter-gap 1.0 0 x))

(define (sqrt-iter-gap guess last-guess x)
  (if (good-enough-gap? guess last-guess x)
      guess
      (sqrt-iter-gap (improve guess x) 
                     guess
                     x)))

(define (good-enough-gap? guess last-guess x)
  (< (abs (- guess last-guess)) (/ guess 100000)))

;;;  Ex 1.8
;;
(define (cube-root x)
  (cube-root-iter 1.0 0 x))

(define (cube-root-iter guess last-guess x)
  (if (good-enough-gap? guess last-guess x)
      guess
      (cube-root-iter (improve-cube guess x) 
                      guess
                      x)))

(define (improve-cube guess x)
  (/ (+ (/ x (sqr guess))
        (* 2 guess))
     3))

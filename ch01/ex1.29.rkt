#lang racket

(define (sum term a next b)
  (if (> a b)
      0
      (+ (term a)
         (sum term (next a) next b))))

(define (inc n) (+ n 1))
(define (identity x) x)
(define (square x) (* x x))
(define (cube x) (* x x x))

(define (sum-cubes a b)
  (sum cube a inc b))

(define (sum-integers a b)
  (sum identity a inc b))

(define (pi-sum a b)
  (define (pi-term x) (/ 1.0 (* x (+ x 2))))
  (define (pi-next x) (+ x 4))
  (sum pi-term a pi-next b))

(define (integral f a b dx)
  (define (add-dx x) (+ x dx))
  (* (sum f (+ a (/ dx 2.0)) add-dx b)
     dx))

(define (divides? a b) (= (remainder b a) 0))
(define (smallest-divisor n)
  (define (find-divisor n test-divisor)
    (cond ((> (sqr test-divisor) n) n)
          ((divides? test-divisor n) test-divisor)
          (else (find-divisor n (+ test-divisor 1)))))
  (find-divisor n 2))

(define (prime? n)
  (= n (smallest-divisor n)))


;;;  Ex 1.29
;;;
; Simpson's rule
; (h/3)[Y0 + 4y1 + 2Y2 + 4Y3 + ... + 4Yn-1 + Yn]
; h  = (b-a)/n  
; Yk = f(a+kh)
(define (simpson-integral f a b n)
  (define (y-term k h)  
    ; lexical scope includes f a b n 
    (let ((y (f (+ a (* k h)))))
      (cond ((or (zero? k)
                 (= k n)) y)
            ((even? k) (* 2 y))
            (else (* 4 y)))))
  (let ((h (/ (- b a) n)))
    (* (/ h 3)
       (sum (lambda (k) (y-term k h)) 0 inc n))))


;;;  Ex 1.30
;;;
(define (sum-iter term a next b)
  (define (iter a result)
    (if (> a b)
        result
        (iter (next a) (+ result (term a)))))
  (iter a 0))

(define (simpson-integral-iter f a b n)
  (define (y-term k h)  
    ; lexical scope includes f a b n 
    (let ((y (f (+ a (* k h)))))
      (cond ((or (zero? k)
                 (= k n)) y)
            ((even? k) (* 2 y))
            (else (* 4 y)))))
  (let ((h (/ (- b a) n)))
    (* (/ h 3)
       (sum-iter (lambda (k) (y-term k h)) 0 inc n))))

;;;  Ex 1.31 a)
;;
(define (product term a next b)
  (if (> a b)
      1
      (* (term a)
         (product term (next a) next b))))


(define (factorial n)
  (cond ((zero? n) 1)
        (else (product identity 1 inc n))))  

(define PI-LIMIT 1000)

(define (almost-pi)
  (define (pi-term k)
    (let* ((numer1 (* 2 k))  
           (denom (add1 numer1))
           (numer2 (add1 denom)))
      (/ (* numer1 numer2)
         (square denom))))
  (* 4.0 (product pi-term 1 inc PI-LIMIT)))

;;;  Ex 1.31 b)
;;
(define (product-iter term a next b)
  (define (iter a result)
    (if (> a b)
        result
        (iter (next a) (* result (term a)))))
  (iter a 1))

(define (almost-pi-iter)
  (define (pi-term k)
    (let* ((numer1 (* 2 k))  
           (denom (add1 numer1))
           (numer2 (add1 denom)))
      (/ (* numer1 numer2)
         (square denom))))
  (* 4.0 (product-iter pi-term 1 inc PI-LIMIT)))

(define (factorial-iter n)
  (cond ((zero? n) 1)
        (else (product-iter identity 1 inc n))))  

;;;  Ex 1.32 a)
;;
(define (accumulate combiner null-value term a next b)
  (if (> a b)
      null-value
      (combiner (term a)
                (accumulate combiner null-value term (next a) next b))))

(define (sum-acc a b)
  (accumulate + 0 identity a inc b))

(define (product-acc a b)
  (accumulate * 1 identity a inc b))

;;;  Ex 1.32 b)
;;
(define (accumulate-iter combiner null-value term a next b)
  (define (iter a result)
    (if (> a b)
        result
        (iter (next a) (combiner result (term a)))))
  (iter a null-value))

(define (sum-acc-iter a b)
  (accumulate-iter + 0 identity a inc b))

(define (product-acc-iter a b)
  (accumulate-iter * 1 identity a inc b))


;;;  Ex 1.33
;;
;(define (prime? n)
;  (define (smallest-divisor n)
;    (define (find-divisor n test-divisor)
;      (define (divides? a b) (= (remainder b a) 0))
;      (cond ((> (square test-divisor) n) n)
;            ((divides? test-divisor n) test-divisor)
;            (else (find-divisor n (+ test-divisor 1)))))
;    (find-divisor n 2))
;  (= n (smallest-divisor n)))

(define (filtered-accumulate pred? combiner null-value term a next b)
  (define (iter a result)
    (if (> a b)
        result
        (iter (next a) (if (pred? a)
                           (combiner result (term a))
                           result))))
  (iter a null-value))

;;;  Ex 1.33 a)
;;
(define (sum-squared-primes a b)
  (filtered-accumulate prime? + 0 square a inc b))

;;;  Ex 1.33 b)
;;
(define (relative-prime-product n)
  (define (relatively-prime? i)
    (define (gcd a b)
      (if (= b 0)
          a
          (gcd b (remainder a b))))
    (= (gcd i n) 1))
  (filtered-accumulate relatively-prime? * 1 identity 0 inc n))


;;;  Ex 1.34
;;
(define (f g) (g 2))

;(f f) results in an error :
; substitution rule
;-> (f 2)
;-> (2 2) -> error expects a procedure but given a number


;;;  Ex 1.35
;;
(define phi-tolerance 0.00001)
(define (close-enough? v1 v2 tolerance)
  (< (abs (- v1 v2)) tolerance))

(define (fixed-point f first-guess)
  (define (try guess)
    (let ((next (f guess)))
      (if (close-enough? guess next phi-tolerance)
          next
          (try next))))
  (try first-guess))

(define (phi)
  (fixed-point (lambda (x) (+ 1 (/ 1 x))) 1.0))

;;;  Ex 1.36
;;
(define (print-fixed-point f first-guess)
  (define (try guess)
    (let ((next (f guess)))
      (printf "~a -> ~a~n" guess next)
      (if (close-enough? guess next phi-tolerance)
          next
          (try next))))
  (try first-guess))

(define (average v1 v2)
  (/ (+ v1 v2) 2))

(define (x^x=1000)
  (let* ((tries 0)
         (solution (print-fixed-point (lambda (x) 
                                        (set! tries (add1 tries)) 
                                        (/ (log 1000) (log x)))
                                      3.0)))
    (printf "~ntook ~a tries~n" tries)
    solution))

(define (x^x=1000-damped)
  (let* ((tries 0)
         (solution (print-fixed-point (lambda (x) 
                                        (set! tries (add1 tries)) 
                                        (average x (/ (log 1000) (log x))))
                                      3.0)))
    (printf "~ntook ~a tries~n" tries)
    solution))


;;;  Ex 1.37 a
;;
(define (cont-frac n d k)
  (cond ((zero? k) 0)
        (else (/ (n k)
                 (+ (d k) (cont-frac n d (sub1 k)))))))

(define 1/phi-tolerance 0.00005)

(define (1/phi)
  (define (try target k)
    (let ((1/phi-k (cont-frac (lambda (i) 1.0)
                              (lambda (i) 1.0)
                              k)))  
      (cond ((close-enough? target 1/phi-k 1/phi-tolerance)
             (newline)
             (display k)
             (display " iterations of cont-frac gives ")
             (newline)
             1/phi-k)
            (else (try target (add1 k))))))
  (try (/ 1 (phi)) 1))

;;;  Ex 1.37 b
;;
(define (cont-frac-iter n d k)
  (define (iter k result)
    (cond ((zero? k) result)
          (else (iter (sub1 k) 
                      (/ (n k)
                         (+ (d k) result))))))
  (iter k 0))

(define (1/phi-iter)
  (define (try target k)
    (let ((1/phi-k (cont-frac-iter (lambda (i) 1.0)
                                   (lambda (i) 1.0)
                                   k)))  
      (cond ((close-enough? target 1/phi-k 1/phi-tolerance)
             (printf "~n~a iterations of cont-frac-iter gives~n" k)
             1/phi-k)
            (else (try target (add1 k))))))
  (try (/ 1 (phi)) 1))

;;;  Ex 1.38
;;
(define (euler-e)
  (+ 2
     (cont-frac-iter (lambda (i) 1.0)
                     (lambda (i) 
                       (if (= (remainder i 3) 2)
                           (* (quotient (add1 i) 3) 2)
                           1))
                     100)))

;;;  Ex 1.39
;;
(define (tan-cf x k)
  (cont-frac-iter (lambda (i) (if (= i 1) x (- (square x))))
                  (lambda (i) (sub1 (* 2 i)))
                  k))


;;;  Ex 1.40
;;
(define (newtons-method g guess)
  (fixed-point (newton-transform g) guess))

(define (newton-transform g)
  (lambda (x)
    (- x (/ (g x) ((deriv g) x)))))

(define (deriv g)
  (lambda (x)
    (/ (- (g (+ x dx)) (g x))
       dx)))

(define dx 0.00001)

(define (cubic a b c)
  (lambda (x)
    (+ (cube x)
       (* a (square x))
       (* b x)
       c)))

;;;  Ex 1.41
;;
(define (double f)
  (lambda (x)
    (f (f x))))

;;;  Ex 1.42
;;
(define (compose f g)
  (lambda (x)
    (f (g x))))

;;;  Ex 1.43
;;
(define (repeated f n)
  (cond ((= n 1) f)
        (else (compose f (repeated f (sub1 n))))))

(define (repeated-iter f n)
  (define (iter k result)
    (cond ((= k 1) result)
          (else (compose f (repeated f (sub1 k))))))
  (iter n f))


;;;  Ex 1.44
;;
(define (smooth f)
  (lambda (x) 
    (/ (+ (f (- x dx))
          (f x)
          (f (+ x dx)))
       3)))

(define (n-fold-smooth f n)
  ((repeated smooth n) f))

(define (smooth-dx f dx)
  (lambda (x) 
    (/ (+ (f (- x dx))
          (f x)
          (f (+ x dx)))
       3)))

(define (n-fold-smooth-dx f dx n)
  ((repeated (lambda (f-x) 
               (smooth-dx f-x dx)) 
             n) f))

;;;  Ex 1.45
;;
(define (sq-root x)
  (fixed-point-of-transform (lambda (y) (/ x y))
                            average-damp
                            1.0))

(define (fixed-point-of-transform g transform guess)
  (fixed-point (transform g) guess))

(define (average-damp f)
  (lambda (x) (average x (f x))))

(define (cube-root x)
  (fixed-point-of-transform (lambda (y) (/ x (square y)))
                            average-damp
                            1.0))
;(define (4th-root x)
;  (fixed-point-of-transform (lambda (y) (/ x (cube y)))
;                            (repeated average-damp 2)
;                            1.0))
;
;(define (7th-root x)
;  (fixed-point-of-transform (lambda (y) (/ x (expt y 6)))
;                            (repeated average-damp 2)
;                            1.0))
;
; I HAZ FAIL
;(define (8th-root x)
;  (fixed-point-of-transform (lambda (y) (/ x (expt y 7)))
;                            (repeated average-damp 2)   ;; use 3
;                            1.0))
;
;(define (15th-root x)
;  (fixed-point-of-transform (lambda (y) (/ x (expt y 14)))
;                            (repeated average-damp 3)
;                            1.0))
;
; I HAZ FAIL
;(define (16th-root x)
;  (fixed-point-of-transform (lambda (y) (/ x (expt y 15)))
;                            (repeated average-damp 3)
;                            1.0))

(define (nth-root x n)
  (define (log2 n) (/ (log n) (log 2)))
  (fixed-point-of-transform (lambda (y) (/ x (expt y (sub1 n))))
                            (repeated average-damp (floor (log2 n)))
                            1.0))

;;;  Ex 1.46
;;
(define (iterative-improve good-enough? improve)
  (lambda (guess)
    (if (good-enough? guess) 
        guess
        ((iterative-improve good-enough? improve) (improve guess)))))

(define (sqrt-iterative x)
  ((iterative-improve (lambda (guess)
                        (< (abs (- (square guess) x)) 0.001))
                      (lambda (guess)
                        (/ (+ guess 
                              (/ x guess))
                           2))) x)) 

(define (fixed-point-iterative f first-guess)
  (define tolerance 0.00001)
  
  ((iterative-improve (lambda (guess)
                        (let ((next (f guess)))
                          (< (abs (- guess next)) tolerance)))
                      f) first-guess))
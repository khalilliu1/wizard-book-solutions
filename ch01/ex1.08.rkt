#lang racket

;;;  Ex 1.8
;;

(define (good-enough-gap? guess last-guess x)
  (< (abs (- guess last-guess)) (/ guess 100000)))

(define (cube-root x)
  (cube-root-iter 1.0 0 x))

(define (cube-root-iter guess last-guess x)
  (if (good-enough-gap? guess last-guess x)
      guess
      (cube-root-iter (improve-cube guess x) 
                      guess
                      x)))

(define (improve-cube guess x)
  (/ (+ (/ x (sqr guess))
        (* 2 guess))
     3))

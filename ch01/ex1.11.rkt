#lang racket

;;;  Ex 1.11
;; f(n) = n if n<3 and f(n) = f(n - 1) + 2f(n - 2) + 3f(n - 3) if n> 3.
;;
(define (f1 n) (f-rec n))
(define (f2 n) 
  (if (< n 3) 
      n
      (f-iter 0 1 2 (- n 2))))

(define (f-rec n)
  (if (< n 3) 
      n
      (+ (* 1 (f-rec (- n 1)))
         (* 2 (f-rec (- n 2)))
         (* 3 (f-rec (- n 3))))))

(define (f-iter a b c count)
  (if (zero? count) 
      c
      (f-iter b 
              c  
              (+ c (* 2 b) (* 3 a))
              (sub1 count))))

